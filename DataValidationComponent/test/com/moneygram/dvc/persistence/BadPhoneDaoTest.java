package com.moneygram.dvc.persistence;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.moneygram.dvc.persistence.BadPhoneDao.BadPhoneMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:/META-INF/spring/dvc-test-component.xml")
public class BadPhoneDaoTest {

	@Autowired
	BadPhoneDao dao;
	
	@Test
	public void testGetRowMapper() {
		RowMapper<?> mapper = dao.getRowMapper();
		Assert.assertNotNull("getRowMapper returned null.", mapper);
		Assert.assertTrue("", mapper instanceof BadPhoneMapper);
	}
	
	@Test
	public void testGetStoredProcedureName() {
		String s = dao.getStoredProcedureName();
		Assert.assertNotNull("getStoredProcedureName returned null.", s);
		Assert.assertEquals(BadPhoneDao.BAD_PHONE_PROC_NAME, s);
	}

	@Test 
	public void testGetProcessLogIdOutputName() {
		String s = dao.getProcessLogIdOutputName();
		Assert.assertNotNull("getProcessLogIdOutputName returned null.", s);
		Assert.assertEquals(BadPhoneDao.PROCESS_LOG_ID, s);
	}

	@Test
	public void testGetBusinessAreaCodeInputName() {
		String s = dao.getBusinessAreaCodeInputName();
		Assert.assertNotNull("getBusinessAreaCodeInputName returned null.", s);
		Assert.assertEquals(BadPhoneDao.BUSINESS_CODE, s);
	}

	@Test
	public void testGetCallTypeCodeInputName() {
		String s = dao.getCallTypeCodeInputName();
		Assert.assertNotNull("getCallTypeCodeInputName returned null.", s);
		Assert.assertEquals(BadPhoneDao.CALL_TYPE_CODE, s);
	}

	@Test
	public void testGetCursorOutputName() {
		String s = dao.getCursorOutputName();
		Assert.assertNotNull("getCursorOutputName returned null.", s);
		Assert.assertEquals(BadPhoneDao.OUTPUT_CURSOR, s);
	}

	@Test
	public void testFind() throws Exception {
		List<?> phones = dao.find();
		
		Assert.assertNotNull("find returned null.", phones);
		Assert.assertFalse("expecting a non empty list", phones.isEmpty());
		Assert.assertTrue("returned incorrect type", phones.get(0) instanceof String);
	}

	@Test
	public void testFindInvalidBusinessAreaCode() throws Exception {
		// Change business area code to some non-existent code.
		dao.setBusinessAreaCode("ZZZXYZ");
		
		List<?> phones = dao.find();

		Assert.assertNotNull("find returned null.", phones);
		Assert.assertTrue("expecting an empty list", phones.isEmpty());
		
	}
}
