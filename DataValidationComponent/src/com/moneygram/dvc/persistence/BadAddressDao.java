package com.moneygram.dvc.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.rule.common.datalist.RuleDataListFacade;

/**
 * BadAddressDao provides the implementation of the {@link RuleDataListFacade}
 * to retrieve the bad address data list for a given business area.
 * <p>
 * The business area code should be set in the Spring configuration when this
 * DAO is configured and added to the Rule Data List Registry.
 * 
 */
public class BadAddressDao extends AbstractBadDataListDao {

	protected static final String BAD_ADD_PROC_NAME = "pkg_dvs_util.prc_bad_address_cv";
	
	protected static final String BUSINESS_CODE  = "iv_bsns_code";
	protected static final String CALL_TYPE_CODE = "iv_call_type_code";
	protected static final String PROCESS_LOG_ID = "ov_prcs_log_id";
	protected static final String OUTPUT_CURSOR  = "ov_bad_name_cv";

	public BadAddressDao() {
		super();
	}

	@Override
	public RowMapper<?> getRowMapper() {
		return new BadAddressMapper();
	}

	@Override
	public String getStoredProcedureName() {
		return BAD_ADD_PROC_NAME;
	}

	@Override
	public String getProcessLogIdOutputName() {
		return PROCESS_LOG_ID;
	}

	@Override
	public String getBusinessAreaCodeInputName() {
		return BUSINESS_CODE;
	}

	@Override
	public String getCallTypeCodeInputName() {
		return CALL_TYPE_CODE;
	}

	@Override
	public String getCursorOutputName() {
		return OUTPUT_CURSOR;
	}

	/**
	 * BadAddressMapper processes a {@link ResultSet} to populate an
	 * {@link AddressBO} object.
	 * 
	 */
	protected static final class BadAddressMapper implements RowMapper<AddressBO> {

		private static final String ADDR_LINE1_COLUMN  = "addr_line1_text";
		private static final String ADDR_LINE2_COLUMN  = "addr_line2_text";
		private static final String ADDR_LINE3_COLUMN  = "addr_line3_text";
		private static final String CITY_COLUMN        = "city_text";
		private static final String STATE_COLUMN       = "state_text";
		private static final String POSTAL_CODE_COLUMN = "postal_code";
		private static final String COUNTRY_COLUMN     = "country_text";

		@Override
		public AddressBO mapRow(ResultSet resultset, int i) throws SQLException {
			AddressBO addressBO = new AddressBO();
			
			addressBO.setAddressLine1(resultset.getString(ADDR_LINE1_COLUMN));
			addressBO.setAddressLine2(resultset.getString(ADDR_LINE2_COLUMN));
			addressBO.setAddressLine3(resultset.getString(ADDR_LINE3_COLUMN));
			addressBO.setCity(resultset.getString(CITY_COLUMN));
			addressBO.setStateProvince(resultset.getString(STATE_COLUMN));
			addressBO.setPostalCode(resultset.getString(POSTAL_CODE_COLUMN));
			addressBO.setIsoCountryCode(resultset.getString(COUNTRY_COLUMN));
			
			return addressBO;
		}
	}
}
