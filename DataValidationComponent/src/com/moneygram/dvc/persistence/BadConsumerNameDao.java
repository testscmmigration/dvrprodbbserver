package com.moneygram.dvc.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.moneygram.dvc.bo.ConsumerNameBO;
import com.moneygram.rule.common.datalist.RuleDataListFacade;

/**
 * BadConsumerNameDao provides the implementation of the
 * {@link RuleDataListFacade} to retrieve the bad consumer name data list for a
 * given business area.
 * <p>
 * The business area code should be set in the Spring configuration when this
 * DAO is configured and added to the Rule Data List Registry.
 * 
 */
public class BadConsumerNameDao extends AbstractBadDataListDao {

	protected static final String BAD_CONSUMER_NAME_PROC_NAME = "pkg_dvs_util.prc_bad_name_cv";
	
	protected static final String BUSINESS_CODE  = "iv_bsns_code";
	protected static final String CALL_TYPE_CODE = "iv_call_type_code";
	protected static final String PROCESS_LOG_ID = "ov_prcs_log_id";
	protected static final String OUTPUT_CURSOR  = "ov_bad_name_cv";
	
	@Override
	public RowMapper<?> getRowMapper() {
		return new BadConsumerNameMapper();
	}

	@Override
	public String getStoredProcedureName() {
		return BAD_CONSUMER_NAME_PROC_NAME;
	}

	@Override
	public String getBusinessAreaCodeInputName() {
		return BUSINESS_CODE;
	}

	@Override
	public String getCallTypeCodeInputName() {
		return CALL_TYPE_CODE;
	}

	@Override
	public String getCursorOutputName() {
		return OUTPUT_CURSOR;
	}

	@Override
	public String getProcessLogIdOutputName() {
		return PROCESS_LOG_ID;
	}

	/**
	 * BadConsumerNameMapper processes a {@link ResultSet} to populate a
	 * {@link ConsumerNameBO} object.
	 * 
	 */
	protected static final class BadConsumerNameMapper implements RowMapper<ConsumerNameBO> {

		private static final String FAMILY_NAME_COLUMN = "cnsmr_fmly_name";
		private static final String GIVEN_NAME_COLUMN  = "cnsmr_gvn_name";

		@Override
		public ConsumerNameBO mapRow(ResultSet resultset, int i) throws SQLException {
			ConsumerNameBO consumerNameBO = new ConsumerNameBO();

			consumerNameBO.setLastName(resultset.getString(FAMILY_NAME_COLUMN));
			consumerNameBO.setFirstName(resultset.getString(GIVEN_NAME_COLUMN));
			
			return consumerNameBO;
		}
	}
}
