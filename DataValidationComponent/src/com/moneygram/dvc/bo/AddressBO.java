package com.moneygram.dvc.bo;

import java.io.Serializable;

/**
 * AddressBO is used to encapsulate a consumer address.
 * <p>
 * This is used as both part of the input parameters to the consumer validation
 * rule manager and as the output of the bad address DAO.
 * 
 */
public class AddressBO implements Serializable {

	private static final long serialVersionUID = -1090270559232815290L;

	private java.lang.String addressLine1;

    private java.lang.String addressLine2;

    private java.lang.String addressLine3;

    private java.lang.String city;

    private java.lang.String stateProvince;
    
    private java.lang.String postalCode;

    private java.lang.String isoCountryCode;
    
    public AddressBO() {
    	super();
    };

	public AddressBO(String addressLine1, String addressLine2, String addressLine3, String city, String stateProvince, String postalCode, String isoCountryCode) {
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.city = city;
		this.stateProvince = stateProvince;
		this.postalCode = postalCode;
		this.isoCountryCode = isoCountryCode;
	}

	public java.lang.String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(java.lang.String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public java.lang.String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(java.lang.String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public java.lang.String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(java.lang.String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public java.lang.String getCity() {
		return city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.String getIsoCountryCode() {
		return isoCountryCode;
	}

	public void setIsoCountryCode(java.lang.String isoCountryCode) {
		this.isoCountryCode = isoCountryCode;
	}

	public java.lang.String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(java.lang.String stateProvince) {
		this.stateProvince = stateProvince;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((addressLine1 == null) ? 0 : addressLine1.hashCode());
		result = PRIME * result + ((addressLine2 == null) ? 0 : addressLine2.hashCode());
		result = PRIME * result + ((addressLine3 == null) ? 0 : addressLine3.hashCode());
		result = PRIME * result + ((city == null) ? 0 : city.hashCode());
		result = PRIME * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = PRIME * result + ((isoCountryCode == null) ? 0 : isoCountryCode.hashCode());
		result = PRIME * result + ((stateProvince == null) ? 0 : stateProvince.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AddressBO other = (AddressBO) obj;
		if (addressLine1 == null) {
			if (other.addressLine1 != null)
				return false;
		} else if (!addressLine1.equals(other.addressLine1))
			return false;
		if (addressLine2 == null) {
			if (other.addressLine2 != null)
				return false;
		} else if (!addressLine2.equals(other.addressLine2))
			return false;
		if (addressLine3 == null) {
			if (other.addressLine3 != null)
				return false;
		} else if (!addressLine3.equals(other.addressLine3))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if(postalCode == null){
			if(other.postalCode != null)
				return false;
		} else if(!postalCode.equals(other.postalCode))
			return false;
		if (isoCountryCode == null) {
			if (other.isoCountryCode != null)
				return false;
		} else if (!isoCountryCode.equals(other.isoCountryCode))
			return false;
		if (stateProvince == null) {
			if (other.stateProvince != null)
				return false;
		} else if (!stateProvince.equals(other.stateProvince))
			return false;
		return true;
	}

	public java.lang.String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(java.lang.String postalCode) {
		this.postalCode = postalCode;
	}


}
