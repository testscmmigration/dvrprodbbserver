/**
 * PersonalID.java
 */

package com.moneygram.dvc.bo;

import java.io.Serializable;

/**
 * PersonalIDBO encapsulates the consumer's personal ID information.
 *
 */
public class PersonalIDBO implements Serializable {
	
	private static final long serialVersionUID = -4509401619579430393L;

	private java.lang.String personalIDType;

    private java.lang.String personalIDNumber;

    private java.lang.String isoCountryCode;

    private java.lang.String stateProvince;

    private String category;
    
    public PersonalIDBO() {
    }

    public PersonalIDBO(
           java.lang.String personalIDType,
           java.lang.String personalIDNumber,
           java.lang.String isoCountryCode,
           java.lang.String stateProvince) {
           this.personalIDType = personalIDType;
           this.personalIDNumber = personalIDNumber;
           this.isoCountryCode = isoCountryCode;
           this.stateProvince = stateProvince;
    }

    public PersonalIDBO(
            java.lang.String personalIDType,
            java.lang.String personalIDNumber,
            java.lang.String isoCountryCode,
            java.lang.String stateProvince,
            String category) {
            this.personalIDType = personalIDType;
            this.personalIDNumber = personalIDNumber;
            this.isoCountryCode = isoCountryCode;
            this.stateProvince = stateProvince;
            this.category = category;
     }
    
    /**
     * Gets the personalIDType value for this PersonalID.
     * 
     * @return personalIDType
     */
    public java.lang.String getPersonalIDType() {
        return personalIDType;
    }


    /**
     * Sets the personalIDType value for this PersonalID.
     * 
     * @param personalIDType
     */
    public void setPersonalIDType(java.lang.String personalIDType) {
        this.personalIDType = personalIDType;
    }


    /**
     * Gets the personalIDNumber value for this PersonalID.
     * 
     * @return personalIDNumber
     */
    public java.lang.String getPersonalIDNumber() {
        return personalIDNumber;
    }


    /**
     * Sets the personalIDNumber value for this PersonalID.
     * 
     * @param personalIDNumber
     */
    public void setPersonalIDNumber(java.lang.String personalIDNumber) {
        this.personalIDNumber = personalIDNumber;
    }


    /**
     * Gets the isoCountryCode value for this PersonalID.
     * 
     * @return isoCountryCode
     */
    public java.lang.String getIsoCountryCode() {
        return isoCountryCode;
    }


    /**
     * Sets the isoCountryCode value for this PersonalID.
     * 
     * @param isoCountryCode
     */
    public void setIsoCountryCode(java.lang.String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }


    /**
     * Gets the stateProvince value for this PersonalID.
     * 
     * @return stateProvince
     */
    public java.lang.String getStateProvince() {
        return stateProvince;
    }


    /**
     * Sets the stateProvince value for this PersonalID.
     * 
     * @param stateProvince
     */
    public void setStateProvince(java.lang.String stateProvince) {
        this.stateProvince = stateProvince;
    }

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((category == null) ? 0 : category.hashCode());
		result = PRIME * result + ((isoCountryCode == null) ? 0 : isoCountryCode.hashCode());
		result = PRIME * result + ((personalIDNumber == null) ? 0 : personalIDNumber.hashCode());
		result = PRIME * result + ((personalIDType == null) ? 0 : personalIDType.hashCode());
		result = PRIME * result + ((stateProvince == null) ? 0 : stateProvince.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PersonalIDBO other = (PersonalIDBO) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (isoCountryCode == null) {
			if (other.isoCountryCode != null)
				return false;
		} else if (!isoCountryCode.equals(other.isoCountryCode))
			return false;
		if (personalIDNumber == null) {
			if (other.personalIDNumber != null)
				return false;
		} else if (!personalIDNumber.equals(other.personalIDNumber))
			return false;
		if (personalIDType == null) {
			if (other.personalIDType != null)
				return false;
		} else if (!personalIDType.equals(other.personalIDType))
			return false;
		if (stateProvince == null) {
			if (other.stateProvince != null)
				return false;
		} else if (!stateProvince.equals(other.stateProvince))
			return false;
		return true;
	}
    
}
