package com.moneygram.dvc.bo;

import java.io.Serializable;

import com.moneygram.rule.common.bo.RuleError;

/**
 * RuleErrorBO contains the details of the rule error.
 * <p>
 * It contains the validationID to correlate the error to the original input
 * object. Also contains the error code, message, priority and offending field.
 * 
 */
public class RuleErrorBO implements Serializable {

	private static final long serialVersionUID = -4640944883717075831L;

	private String validationID;
	
	private RuleError ruleError;
	
	public RuleErrorBO(String validationID, RuleError ruleError ) { 
		super();
		this.validationID = validationID;
		this.ruleError = ruleError;
	}

	/**
	 * @return the validationID
	 */
	public String getValidationID() {
		return validationID;
	}

	/**
	 * @return the ruleError
	 */
	public RuleError getRuleError() {
		return ruleError;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ruleError == null) ? 0 : ruleError.hashCode());
		result = prime * result
				+ ((validationID == null) ? 0 : validationID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RuleErrorBO other = (RuleErrorBO) obj;
		if (ruleError == null) {
			if (other.ruleError != null) {
				return false;
			}
		} else if (!ruleError.equals(other.ruleError)) {
			return false;
		}
		if (validationID == null) {
			if (other.validationID != null) {
				return false;
			}
		} else if (!validationID.equals(other.validationID)) {
			return false;
		}
		return true;
	}

	
}
