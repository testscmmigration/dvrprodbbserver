package com.moneygram.dvc.consumervalidation;

import java.util.List;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.ConsumerNameBO;
import com.moneygram.rule.framework.IRuleSetInput;

/**
 * ConsumerValidationRuleSetInput contains the information required by the
 * consumer validation rules to determine the validity of a consumer.
 * 
 */
public class ConsumerValidationRuleSetInput implements IRuleSetInput {

	private ConsumerBO consumer;
	
	private List<String> badPhoneList;
	
	private List<AddressBO> badAddressList;
	
	private List<ConsumerNameBO> badConsumerNameList;

	public ConsumerBO getConsumer() {
		return this.consumer;
	}
	
	public void setConsumer(ConsumerBO consumer) {
		this.consumer = consumer;
	}
	
	public List<String> getBadPhoneList() {
		return this.badPhoneList;
	}
	
	public void setBadPhoneList(List<String> badPhoneList) {
		this.badPhoneList = badPhoneList;
	}
	
	public List<AddressBO> getBadAddressList() {
		return this.badAddressList;
	}
	
	public void setBadAddressList(List<AddressBO> badAddressList) {
		this.badAddressList = badAddressList;
	}

	public List<ConsumerNameBO> getBadConsumerNameList() {
		return badConsumerNameList;
	}
	
	public void setBadConsumerNameList(List<ConsumerNameBO> badConsumerNameList) {
		this.badConsumerNameList = badConsumerNameList;
	}
}
