
package com.moneygram.service.dvr_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceResponseMessage;


/**
 * <p>Java class for ConsumerValidationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerValidationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceResponseMessage">
 *       &lt;sequence>
 *         &lt;element name="validationErrors" type="{http://moneygram.com/service/DVR_v1}ArrayOfErrorDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerValidationResponse", propOrder = {
    "validationErrors"
})
@XmlRootElement(name = "consumerValidationReturn")
public class ConsumerValidationReturn
    extends BaseServiceResponseMessage
{

    protected ArrayOfErrorDetail validationErrors;

    /**
     * Gets the value of the validationErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorDetail }
     *     
     */
    public ArrayOfErrorDetail getValidationErrors() {
        return validationErrors;
    }

    /**
     * Sets the value of the validationErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorDetail }
     *     
     */
    public void setValidationErrors(ArrayOfErrorDetail value) {
        this.validationErrors = value;
    }

}
