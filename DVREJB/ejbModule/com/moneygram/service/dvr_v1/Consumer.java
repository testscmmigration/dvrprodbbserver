
package com.moneygram.service.dvr_v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter2;


/**
 * <p>Java class for Consumer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Consumer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validationID" type="{http://moneygram.com/service/DVR_v1}ValidationID" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://moneygram.com/service/DVR_v1}ConsumerName" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://moneygram.com/service/DVR_v1}ConsumerLastName" minOccurs="0"/>
 *         &lt;element name="secondLastName" type="{http://moneygram.com/service/DVR_v1}ConsumerLastName" minOccurs="0"/>
 *         &lt;element name="middleName" type="{http://moneygram.com/service/DVR_v1}ConsumerName" minOccurs="0"/>
 *         &lt;element name="addressLine1" type="{http://moneygram.com/service/DVR_v1}AddressLine" minOccurs="0"/>
 *         &lt;element name="addressLine2" type="{http://moneygram.com/service/DVR_v1}AddressLine" minOccurs="0"/>
 *         &lt;element name="addressLine3" type="{http://moneygram.com/service/DVR_v1}AddressLine" minOccurs="0"/>
 *         &lt;element name="city" type="{http://moneygram.com/service/DVR_v1}City" minOccurs="0"/>
 *         &lt;element name="postalCode" type="{http://moneygram.com/service/DVR_v1}PostalCode" minOccurs="0"/>
 *         &lt;element name="stateProvince" type="{http://moneygram.com/service/DVR_v1}StateProvince" minOccurs="0"/>
 *         &lt;element name="cellPhoneNumber" type="{http://moneygram.com/service/DVR_v1}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="homePhoneNumber" type="{http://moneygram.com/service/DVR_v1}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="isoCountryCode" type="{http://moneygram.com/service/DVR_v1}ISOCountryCode" minOccurs="0"/>
 *         &lt;element name="birthDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="personalID1" type="{http://moneygram.com/service/DVR_v1}PersonalID" minOccurs="0"/>
 *         &lt;element name="personalID2" type="{http://moneygram.com/service/DVR_v1}PersonalID" minOccurs="0"/>
 *         &lt;element name="personalID3" type="{http://moneygram.com/service/DVR_v1}PersonalID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Consumer", propOrder = {
    "validationID",
    "firstName",
    "lastName",
    "secondLastName",
    "middleName",
    "addressLine1",
    "addressLine2",
    "addressLine3",
    "city",
    "postalCode",
    "stateProvince",
    "cellPhoneNumber",
    "homePhoneNumber",
    "isoCountryCode",
    "birthDate",
    "personalID1",
    "personalID2",
    "personalID3"
})
public class Consumer {

    protected String validationID;
    protected String firstName;
    protected String lastName;
    protected String secondLastName;
    protected String middleName;
    protected String addressLine1;
    protected String addressLine2;
    protected String addressLine3;
    protected String city;
    protected String postalCode;
    protected String stateProvince;
    protected String cellPhoneNumber;
    protected String homePhoneNumber;
    protected String isoCountryCode;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "date")
    protected Calendar birthDate;
    protected PersonalID personalID1;
    protected PersonalID personalID2;
    protected PersonalID personalID3;

    /**
     * Gets the value of the validationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationID() {
        return validationID;
    }

    /**
     * Sets the value of the validationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationID(String value) {
        this.validationID = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the secondLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondLastName() {
        return secondLastName;
    }

    /**
     * Sets the value of the secondLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondLastName(String value) {
        this.secondLastName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the addressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * Sets the value of the addressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine1(String value) {
        this.addressLine1 = value;
    }

    /**
     * Gets the value of the addressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Sets the value of the addressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine2(String value) {
        this.addressLine2 = value;
    }

    /**
     * Gets the value of the addressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    /**
     * Sets the value of the addressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine3(String value) {
        this.addressLine3 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the stateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Sets the value of the stateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateProvince(String value) {
        this.stateProvince = value;
    }

    /**
     * Gets the value of the cellPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    /**
     * Sets the value of the cellPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellPhoneNumber(String value) {
        this.cellPhoneNumber = value;
    }

    /**
     * Gets the value of the homePhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    /**
     * Sets the value of the homePhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomePhoneNumber(String value) {
        this.homePhoneNumber = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsoCountryCode(String value) {
        this.isoCountryCode = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthDate(Calendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the personalID1 property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalID }
     *     
     */
    public PersonalID getPersonalID1() {
        return personalID1;
    }

    /**
     * Sets the value of the personalID1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalID }
     *     
     */
    public void setPersonalID1(PersonalID value) {
        this.personalID1 = value;
    }

    /**
     * Gets the value of the personalID2 property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalID }
     *     
     */
    public PersonalID getPersonalID2() {
        return personalID2;
    }

    /**
     * Sets the value of the personalID2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalID }
     *     
     */
    public void setPersonalID2(PersonalID value) {
        this.personalID2 = value;
    }

    /**
     * Gets the value of the personalID3 property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalID }
     *     
     */
    public PersonalID getPersonalID3() {
        return personalID3;
    }

    /**
     * Sets the value of the personalID3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalID }
     *     
     */
    public void setPersonalID3(PersonalID value) {
        this.personalID3 = value;
    }

}
