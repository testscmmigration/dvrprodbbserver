package com.moneygram.dvr.service.constants;


public class DVRConstants {
	
	//Sender Fields
	public static final String SENDER_FIRST_NAME = "senderFirstName";
	public static final String SENDER_LAST_NAME = "senderLastName";
	public static final String SENDER_MIDDLE_NAME = "senderMiddleName";
	public static final String SENDER_LAST_NAME_2 = "senderLastName2";
	public static final String SENDER_ADDRESS_1 = "senderAddress";
	public static final String SENDER_ADDRESS_2 = "senderAddress2";
	public static final String SENDER_ADDRESS_3 = "senderAddress3";
	public static final String SENDER_CITY="senderCity";
	public static final String SENDER_STATE = "senderState";
	public static final String SENDER_POSTAL_CODE = "senderPostalCode";
	public static final String SENDER_COUNTRY = "senderCountry";
	public static final String SENDER_DOB = "senderDOB";
	public static final String SENDER_PRIMARY_PHONE = "senderPrimaryPhone";
	public static final String SENDER_PRIMARY_PHONE_TYPE = "senderPrimaryPhoneType";
	public static final String SENDER_SECONDARY_PHONE = "senderSecondaryPhone";
	public static final String SENDER_SECONDARY_PHONE_TYPE = "senderSecondaryPhoneType";
	public static final String SENDER_PERSONAL_ID1_NUMBER = "senderPersonalId1Number";
	public static final String SENDER_PERSONAL_ID1_TYPE = "senderPersonalId1Type";
	public static final String SENDER_PERSONAL_ID1_COUNTRY = "senderPersonalId1IssueCountry";
	public static final String SENDER_PERSONAL_ID1_STATE = "senderPersonalId1State";
	public static final String SENDER_PERSONAL_ID2_NUMBER = "senderPersonalId2Number";
	public static final String SENDER_PERSONAL_ID2_TYPE = "senderPersonalId2Type";
	public static final String SENDER_PERSONAL_ID2_COUNTRY = "senderPersonalId2IssueCountry";
	public static final String SENDER_PERSONAL_ID2_STATE = "senderPersonalId2State";
	
	public static final String SENDER_PERSONAL_ID3_NUMBER = "senderPersonalId3Number";
	public static final String SENDER_PERSONAL_ID3_TYPE = "senderPersonalId3Type";
	public static final String SENDER_PERSONAL_ID3_COUNTRY = "senderPersonalId3IssueCountry";
	public static final String SENDER_PERSONAL_ID3_STATE = "senderPersonalId3State";
	
	//Receiver Fields

	public static final String RECEIVER_FIRST_NAME = "receiverFirstName";
	public static final String RECEIVER_LAST_NAME = "receiverLastName";
	public static final String RECEIVER_MIDDLE_NAME = "receiverMiddleName";
	public static final String RECEIVER_LAST_NAME_2 = "receiverLastName2";
	public static final String RECEIVER_ADDRESS_1 = "receiverAddress";
	public static final String RECEIVER_ADDRESS_2 = "receiverAddress2";
	public static final String RECEIVER_ADDRESS_3 = "receiverAddress3";
	public static final String RECEIVER_CITY="receiverCity";
	public static final String RECEIVER_STATE = "receiverState";
	public static final String RECEIVER_POSTAL_CODE = "receiverPostalCode";
	public static final String RECEIVER_COUNTRY = "receiverCountry";
	public static final String RECEIVER_DOB = "receiverDOB";
	public static final String RECEIVER_PRIMARY_PHONE = "receiverPrimaryPhone";
	public static final String RECEIVER_PRIMARY_PHONE_TYPE = "receiverPrimaryPhoneType";
	public static final String RECEIVER_SECONDARY_PHONE = "receiverSecondaryPhone";
	public static final String RECEIVER_SECONDARY_PHONE_TYPE = "receiverSecondaryPhoneType";
	public static final String RECEIVER_PERSONAL_ID1_NUMBER = "receiverPersonalId1Number";
	public static final String RECEIVER_PERSONAL_ID1_TYPE = "receiverPersonalId1Type";
	public static final String RECEIVER_PERSONAL_ID1_COUNTRY = "receiverPersonalId1IssueCountry";
	public static final String RECEIVER_PERSONAL_ID1_STATE = "receiverPersonalId1State";
	public static final String RECEIVER_PERSONAL_ID2_NUMBER = "receiverPersonalId2Number";
	public static final String RECEIVER_PERSONAL_ID2_TYPE = "receiverPersonalId2Type";
	public static final String RECEIVER_PERSONAL_ID2_COUNTRY = "receiverPersonalId2IssueCountry";
	public static final String RECEIVER_PERSONAL_ID2_STATE = "receiverPersonalId2State";
	
	public static final String RECEIVER_PERSONAL_ID3_NUMBER = "receiverPersonalId3Number";
	public static final String RECEIVER_PERSONAL_ID3_TYPE = "receiverPersonalId3Type";
	public static final String RECEIVER_PERSONAL_ID3_COUNTRY = "receiverPersonalId3IssueCountry";
	public static final String RECEIVER_PERSONAL_ID3_STATE = "receiverPersonalId3State";
	
	// Third Party fields
	
	public static final String TP_SENDER_FIRST_NAME = "thirdPartySenderFirstName";
	public static final String TP_SENDER_LAST_NAME = "thirdPartySenderLastName";
	public static final String TP_SENDER_MIDDLE_NAME = "thirdPartySenderMiddleName";
	public static final String TP_SENDER_LAST_NAME_2 = "thirdPartySenderLastName2";
	public static final String TP_SENDER_ADDRESS_1 = "thirdPartySenderAddress";
	public static final String TP_SENDER_ADDRESS_2 = "thirdPartySenderAddress2";
	public static final String TP_SENDER_ADDRESS_3 = "thirdPartySenderAddress3";
	public static final String TP_SENDER_CITY = "thirdPartySenderCity";
	public static final String TP_SENDER_STATE = "thirdPartySenderState";
	public static final String TP_SENDER_POSTAL_CODE = "thirdPartySenderPostalCode";
	public static final String TP_SENDER_COUNTRY = "thirdPartySenderCountry";
	public static final String TP_SENDER_DOB = "thirdPartySenderDOB";
	public static final String TP_SENDER_PRIMARY_PHONE = "thirdPartySenderPrimaryPhone";
	public static final String TP_SENDER_PRIMARY_PHONE_TYPE = "thirdPartySenderPrimaryPhoneType";
	public static final String TP_SENDER_SECONDARY_PHONE = "thirdPartySenderSecondaryPhone";
	public static final String TP_SENDER_SECONDARY_PHONE_TYPE = "thirdPartySenderSecondaryPhoneType";
	public static final String TP_SENDER_PERSONAL_ID1_NUMBER = "thirdPartySenderPersonalId1Number";
	public static final String TP_SENDER_PERSONAL_ID1_TYPE = "thirdPartySenderPersonalId1Type";
	public static final String TP_SENDER_PERSONAL_ID1_COUNTRY = "thirdPartySenderPersonalId1IssueCountry";
	public static final String TP_SENDER_PERSONAL_ID1_STATE = "thirdPartySenderPersonalId1State";
	public static final String TP_SENDER_PERSONAL_ID2_NUMBER = "thirdPartySenderPersonalId2Number";
	public static final String TP_SENDER_PERSONAL_ID2_TYPE = "thirdPartySenderPersonalId2Type";
	public static final String TP_SENDER_PERSONAL_ID2_COUNTRY = "thirdPartySenderPersonalId2IssueCountry";
	public static final String TP_SENDER_PERSONAL_ID2_STATE = "thirdPartySenderPersonalId2State";
	
	public static final String TP_SENDER_PERSONAL_ID3_NUMBER = "thirdPartySenderPersonalId3Number";
	public static final String TP_SENDER_PERSONAL_ID3_TYPE = "thirdPartySenderPersonalId3Type";
	public static final String TP_SENDER_PERSONAL_ID3_COUNTRY = "thirdPartySenderPersonalId3IssueCountry";
	public static final String TP_SENDER_PERSONAL_ID3_STATE = "thirdPartySenderPersonalId3State";
	
	public static final String SENDER="Sender";
	public static final String THIRDPARTY="ThirdParty";
	public static final String RECEIVER="Receiver";
	
	//CONSUMER MAP DETAILS FOR OFFENDING FIELD
	
	public static final String CONSUMER_FIRST_NAME="firstName";
	public static final String CONSUMER_LAST_NAME="lastName";
	public static final String CONSUMER_MIDDLE_NAME="middleName";
	public static final String CONSUMER_SECOND_LAST_NAME="secondLastName";
	public static final String CONSUMER_ADDRESSLINE_1="addressLine1";
	public static final String CONSUMER_ADDRESSLINE_2="addressLine2";
	public static final String CONSUMER_ADDRESSLINE_3="addressLine3";
	public static final String CONSUMER_ISO_COUNTRY_CODE="isoCountryCode";
	public static final String CONSUMER_CITY="city";
	public static final String CONSUMER_POSTAL_CODE="postalCode";
	public static final String CONSUMER_STATE_PROVINCE="stateProvince";
	public static final String CONSUMER_CELL_PHONE_NUMBER="cellPhoneNumber";
	public static final String CONSUMER_CELL_PHONE_NUMBER_TYPE="cellPhoneNumberType";
	public static final String CONSUMER_HOME_PHONE_NUMBER="homePhoneNumber";
	public static final String CONSUMER_HOME_PHONE_NUMBER_TYPE="homePhoneNumberType";
	public static final String CONSUMER_BIRTH_DATE="birthDate";
	public static final String CONSUMER_PERSONAL_ID_1="personalID1";
	public static final String CONSUMER_PERSONAL_ID_2="personalID2";
	public static final String CONSUMER_PERSONAL_ID_3="personalID3";
	
	public static final String CONSUMER_PERSONAL_ID_1_TYPE="personalID1Type";
	public static final String CONSUMER_PERSONAL_ID_1_NUMBER="personalID1Number";
	public static final String CONSUMER_PERSONAL_ID_1_COUNTRYCODE="personalID1IsoCountryCode";
	public static final String CONSUMER_PERSONAL_ID_1_STATEPROVINCE="personalID1StateProvince";
	
	public static final String CONSUMER_PERSONAL_ID_2_TYPE="personalID2Type";
	public static final String CONSUMER_PERSONAL_ID_2_NUMBER="personalID2Number";
	public static final String CONSUMER_PERSONAL_ID_2_COUNTRYCODE="personalID2IsoCountryCode";
	public static final String CONSUMER_PERSONAL_ID_2_STATEPROVINCE="personalID2StateProvince";
	
	public static final String CONSUMER_PERSONAL_ID_3_TYPE="personalID3Type";
	public static final String CONSUMER_PERSONAL_ID_3_NUMBER="personalID3Number";
	public static final String CONSUMER_PERSONAL_ID_3_COUNTRYCODE="personalID3IsoCountryCode";
	public static final String CONSUMER_PERSONAL_ID_3_STATEPROVINCE="personalID3StateProvince";
	
	public static final String FAILED_TO_VALIDATE_CONSUMERS = "Failed to Validate Consumers.";
	public static final String VALIDTE_CONSUMERS = "validateConsumers";
	public static final String DVR_SOURCE_NAME = "DVS";
	public static final String ESB_FUNCTION_NAME_DVR = "DVR";
	
}
