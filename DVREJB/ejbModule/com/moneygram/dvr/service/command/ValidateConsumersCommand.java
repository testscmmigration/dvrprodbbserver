package com.moneygram.dvr.service.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.moneygram.common.guid.RequestGUID;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.perfmon.PerformanceMonitor;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.Error;
import com.moneygram.common_v1.ErrorCategoryCode;
import com.moneygram.common_v1.ErrorHandlingCode;
import com.moneygram.common_v1.Errors;
import com.moneygram.domain.AttributeBO;
import com.moneygram.domain.ValidationErrorBO;
import com.moneygram.domain.ValidationErrorsBO;
import com.moneygram.domain.ValidationRequestBO;
import com.moneygram.dvr.service.constants.DVRConstants;
import com.moneygram.dvr.service.utility.AttributeXmlMapper;
import com.moneygram.dvr.service.utility.ConsumerAttributes;
import com.moneygram.dvr.service.utility.ConsumerMapper;
import com.moneygram.dvr.service.utility.ValidateConsumersBO;
import com.moneygram.service.dvr_v1.ArrayOfErrorDetail;
import com.moneygram.service.dvr_v1.Consumer;
import com.moneygram.service.dvr_v1.ConsumerValidationRequest;
import com.moneygram.service.dvr_v1.ConsumerValidationReturn;
import com.moneygram.service.dvr_v1.ErrorDetail;
import com.moneygram.service.dvr_v1.ObjectFactory;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.services.RequestValidationProcessingService;

public class ValidateConsumersCommand extends ReadCommand implements InitializingBean {


	private static Logger log = LogFactory.getInstance().getLogger(ValidateConsumersCommand.class);
	private RequestValidationProcessingService rvps;
	
	private String dvrFunctionName;
	PerformanceMonitor performanceMonitor;
	RequestGUID requestGUID=null;
	private String attributeXml;
	static Map<String,Map<String,String>> consumerMap;/* = new HashMap<String,Map<String,String>>();*/
	static ConsumerAttributes consumerAttributes = new ConsumerAttributes();
	
	/*private String senderValidationId;
	private String receiverValidationId;
	private String thirdPartyValidationId;
	
	

	public String getSenderValidationId() {
		return senderValidationId;
	}
	public void setSenderValidationId(String senderValidationId) {
		this.senderValidationId = senderValidationId;
	}
	public String getReceiverValidationId() {
		return receiverValidationId;
	}
	public void setReceiverValidationId(String receiverValidationId) {
		this.receiverValidationId = receiverValidationId;
	}
	public String getThirdPartyValidationId() {
		return thirdPartyValidationId;
	}
	public void setThirdPartyValidationId(String thirdPartyValidationId) {
		this.thirdPartyValidationId = thirdPartyValidationId;
	}*/
	/*public Map<String, String> getSenderMap() {
		return senderMap;
	}
	public Map<String, String> getReceiverMap() {
		return receiverMap;
	}
	public Map<String, String> getThirdPartyMap() {
		return thirdPartyMap;
	}*/
	public RequestValidationProcessingService getRvps() {
		return rvps;
	}
	public void setRvps(RequestValidationProcessingService rvps) {
		this.rvps = rvps;
	}

	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return objectFactory.createConsumerValidationReturn();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
	throws CommandException {
		return request instanceof ConsumerValidationRequest;
	}



	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
	throws CommandException {
		performanceMonitor = PerformanceMonitor.getInstance();
		requestGUID = RequestGUID.createRequestGUID("DVR", null);
		performanceMonitor.startRequest(requestGUID);
		ValidateConsumersBO validateConsumersBO = new ValidateConsumersBO();
		ConsumerValidationReturn response =	objectFactory.createConsumerValidationReturn();
		response.setHeader(request.getHeader());
		
		ValidateConsumersBO validateConsumers= createValidationRequest(request,attributeXml,validateConsumersBO);
		try {
			ValidationRequestBO validateRequest = validateConsumers.getValidateRequest();
			validateRequest.setRequestGUID(requestGUID);
			ValidationErrorsBO validationErrorsBO= rvps.invokeRequestValidationService(validateRequest);
			List<ValidationErrorBO> errorsBO=validationErrorsBO.getValidationErrorsBO();

			if (errorsBO != null && !errorsBO.isEmpty()) {

				response.setValidationErrors(createErrorDetailResponse(errorsBO,validateConsumers));
			}

		} catch (Exception e) {
			log.error(DVRConstants.FAILED_TO_VALIDATE_CONSUMERS, e);
			throw new SystemException(DVRConstants.FAILED_TO_VALIDATE_CONSUMERS, e);
		}
		finally{
			if (performanceMonitor != null) {				
				performanceMonitor.finishRequest(requestGUID);
				requestGUID = null;
			}
		}
		log.debug("The validation response is "+response);
		return response;
	}

	/* creates a validationRequestBO based on the input request to DVR
	 * mapAttributes method maps the corresponding consumer object to the attributeBO
	 */
	protected ValidateConsumersBO createValidationRequest(
			BaseServiceRequestMessage request,String attributeXml,ValidateConsumersBO validateConsumersBO) {

		log.debug("Inside method createValidationRequest");
		
		performanceMonitor.addCall(requestGUID, "Create Validation Request");
		ConsumerValidationRequest CVRRequest = (ConsumerValidationRequest) request;
		ValidationRequestBO validationRequest = new ValidationRequestBO();
		List<AttributeBO> attributeBOs = new ArrayList<AttributeBO>();
		if(consumerMap==null){
			consumerMap = AttributeXmlMapper.externalData(attributeXml);
			if(consumerMap!=null){
				consumerAttributes.setConsumerAttributes(consumerMap);
			}
		}

		for (Consumer consumer : CVRRequest.getConsumers().getItems()) {
			if (DVRConstants.SENDER.equalsIgnoreCase(consumer.getValidationID().trim())) {
				Map<String, String> senderMap= new HashMap<String, String>();
				
				//setSenderValidationId(consumer.getValidationID().trim());
				senderMap = ConsumerMapper.createSenderMap(consumer,attributeXml,consumerAttributes);
				validateConsumersBO.setSenderValidationId(consumer.getValidationID().trim());
				validateConsumersBO.setSenderMap(senderMap);
				attributeBOs=ConsumerMapper.mapAttributes(senderMap,attributeBOs);

			} else if (DVRConstants.RECEIVER.equalsIgnoreCase(consumer.getValidationID().trim())) {
				
				Map<String, String> receiverMap= new HashMap<String, String>();
				
				
				
				//setReceiverValidationId(consumer.getValidationID().trim());
				
				receiverMap = ConsumerMapper.createReceiverMap(consumer,attributeXml,consumerAttributes);
				validateConsumersBO.setReceiverValidationId(consumer.getValidationID().trim());
				validateConsumersBO.setReceiverMap(receiverMap);
				attributeBOs=ConsumerMapper.mapAttributes(receiverMap,attributeBOs);
			} else if (DVRConstants.THIRDPARTY.equalsIgnoreCase(consumer.getValidationID().trim())) {
				Map<String,String> thirdPartyMap=new HashMap<String, String>();
				
				
				//setThirdPartyValidationId(consumer.getValidationID().trim());
				thirdPartyMap = ConsumerMapper.createThirdPartyMap(consumer,attributeXml,consumerAttributes);
				validateConsumersBO.setThirdPartyValidationId(consumer.getValidationID().trim());
				validateConsumersBO.setThirdPartyMap(thirdPartyMap);
				attributeBOs=ConsumerMapper.mapAttributes(thirdPartyMap,attributeBOs);
			}

		}
		validationRequest.setEsbFunctionName(DVRConstants.ESB_FUNCTION_NAME_DVR);
		validationRequest.setAttributes(attributeBOs);
		validationRequest.setOdmFunctionName(dvrFunctionName);
		log.debug("DVR Function Name::"+validationRequest.getOdmFunctionName());
		log.info(" The validation request is "+validationRequest);
		validateConsumersBO.setValidateRequest(validationRequest);
		performanceMonitor.finishCall(requestGUID, "Create Validation Request");
		return validateConsumersBO;

	}


	// creates the error object and maps the RVS validations errors
	protected ArrayOfErrorDetail createErrorDetailResponse(List<ValidationErrorBO> validationErrorBOList,ValidateConsumersBO validateConsumers)
	{
		log.info("Inside method createErrorDetailResponse");
		Map<String, ErrorDetail> errorMap = new HashMap<String, ErrorDetail>();
		String validationID=null;
		for(ValidationErrorBO validationErrorBO:validationErrorBOList)
		{
			ErrorDetail errorDetail;
			// Validation ID is mapped using Offending field.
			String offendingField= validationErrorBO.getError().getOffendingField();
			if(null!=validateConsumers.getSenderMap() && validateConsumers.getSenderMap().containsKey(offendingField))
			{
				validationID= validateConsumers.getSenderValidationId();
			}
			else if(null!=validateConsumers.getReceiverMap() && validateConsumers.getReceiverMap().containsKey(offendingField))
			{
				validationID=validateConsumers.getReceiverValidationId();
			}
			else if(null!=validateConsumers.getThirdPartyMap() && validateConsumers.getThirdPartyMap().containsKey(offendingField))
			{
				validationID=validateConsumers.getThirdPartyValidationId();
			}

			if(!errorMap.containsKey(validationID))
			{
				errorDetail= new ErrorDetail();
				errorDetail.setErrors(new Errors());
				errorDetail.setValidationID(validationID);
				errorMap.put(validationID, errorDetail);

			}

			errorDetail= errorMap.get(validationID);
			errorDetail.getErrors().getErrors().add(createErrorAttr(validationErrorBO));

		}

		ArrayOfErrorDetail arrayOfErrorDetail = new ArrayOfErrorDetail();
		arrayOfErrorDetail.getItems().addAll(errorMap.values());
		log.info("Leaving method createErrorDetailResponse");
		return arrayOfErrorDetail;
	}

	// Error elements from RVS is set to DVR elements
	protected Error createErrorAttr(ValidationErrorBO validateErrorBO)
	{
		Error dest= new Error();
		dest.setErrorCategoryCode(ErrorCategoryCode.USER_ERROR);
		dest.setErrorHandlingCode(ErrorHandlingCode.RETURN_ERROR);
		dest.setErrorLocation(DVRConstants.VALIDTE_CONSUMERS);
		dest.setErrorMessage(validateErrorBO.getError().getErrorMessage());
		dest.setErrorCode(validateErrorBO.getError().getErrorCode());
		dest.setErrorSource(DVRConstants.DVR_SOURCE_NAME);
		String offendingFieldDVR=ConsumerMapper.getOffendingFieldMap().get(validateErrorBO.getError().getOffendingField()); 
		dest.setOffendingField(offendingFieldDVR);
		return dest;

	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(objectFactory, "objectFactory is null.");
	}

	public void setObjectFactory(ObjectFactory objectFactory) {
		this.objectFactory = objectFactory;
	}

	private ObjectFactory objectFactory;

	public String getDvrFunctionName() {
		return dvrFunctionName;
	}
	public void setDvrFunctionName(String dvrFunctionName) {
		this.dvrFunctionName = dvrFunctionName;
	}
	public String getAttributeXml() {
		return attributeXml;
	}
	public void setAttributeXml(String attributeXml) {
		this.attributeXml = attributeXml;
	}
}
