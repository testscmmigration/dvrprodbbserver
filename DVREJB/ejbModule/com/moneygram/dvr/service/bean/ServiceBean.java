package com.moneygram.dvr.service.bean;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import javax.xml.transform.Source;
import javax.xml.ws.Provider;
import javax.xml.ws.Service.Mode;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;



import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.ApplicationContextFactory;
import com.moneygram.common.util.ApplicationContextProvider;
import com.moneygram.exception.RequestValidationServiceException;
import com.moneygram.service.framework.BaseService;
import com.moneygram.service.framework.bo.EJBResponse;
import com.moneygram.service.framework.router.RequestRouter;

/**
 * Session Bean implementation class ServiceBean
 */
@Stateless
@ServiceMode(value = Mode.PAYLOAD)
@WebServiceProvider(targetNamespace = "http://moneygram.com/service/DVR_v1", serviceName = "DVR", portName = "DVRPort", wsdlLocation = "META-INF/wsdl/DVR_v1.wsdl" )
public class ServiceBean extends BaseService implements ServiceBeanLocal, Provider<Source> {

	@Resource
	SessionContext ctx;
	/*@Resource(name="jdbc/MGTDataSource")private DataSource mgtDataSource;*/
	@Resource(name="jdbc/TPEDataSource")private DataSource tpeDataSource;
	
	
	private static Logger logger = LogFactory.getInstance().getLogger(ServiceBean.class);
	
    /**
     * @see BaseService#BaseService()
     */
    public ServiceBean() {
        super();
    }

	@Override
	public Source invoke(Source request) {
		return super.invoke(request);
	}

	@Override
	protected String processRequest(String s) {
		if (logger.isInfoEnabled()) {
			logger.info(String.format("processRequest: request = %s", s));
		}
		
		if(ApplicationContextProvider.getApplicationContext()==null){
			ApplicationContextFactory.getApplicationContext();
		}
		RequestRouter router = (RequestRouter) ApplicationContextProvider.getRootBean();
		
		EJBResponse response = router.process(s);
		if (response.isRollback()) {
			logger.error(String.format("Rollback requested for request:\n %s \n error:\n %s", s, response.getResponsePayload()));
			ctx.setRollbackOnly();
		}
		
		if (logger.isInfoEnabled()) {
			logger.info(String.format("processRequest: response payload = %s", response.getResponsePayload()));
		}
		
		if (response.isReturnResponseAsException()) {
			return getSoapFault(response.getResponsePayload());
		}
		
		return response.getResponsePayload();
	}

}
