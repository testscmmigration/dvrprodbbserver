package com.moneygram.dvr.service.utility;

import java.util.Map;

public class ConsumerAttributes {
	Map<String,Map<String,String>> consumerAttributes;
	public Map<String, Map<String, String>> getConsumerAttributes() {
		return consumerAttributes;
	}
	public void setConsumerAttributes(
			Map<String, Map<String, String>> consumerAttributes) {
		this.consumerAttributes = consumerAttributes;
	}

}
