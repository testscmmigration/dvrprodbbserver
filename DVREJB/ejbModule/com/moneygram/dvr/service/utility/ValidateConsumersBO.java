package com.moneygram.dvr.service.utility;

import java.util.Map;

import com.moneygram.domain.ValidationRequestBO;
import com.moneygram.validation.request.ValidationRequest;

public class ValidateConsumersBO {

	private ValidationRequestBO validateRequest;
	private Map<String,String> senderMap;
	private Map<String,String> receiverMap;
	private Map<String,String> thirdPartyMap;
	private String senderValidationId;
	private String receiverValidationId;
	private String thirdPartyValidationId;
	public ValidationRequestBO getValidateRequest() {
		return validateRequest;
	}
	public void setValidateRequest(ValidationRequestBO validateRequest) {
		this.validateRequest = validateRequest;
	}
	public Map<String, String> getSenderMap() {
		return senderMap;
	}
	public void setSenderMap(Map<String, String> senderMap) {
		this.senderMap = senderMap;
	}
	public Map<String, String> getReceiverMap() {
		return receiverMap;
	}
	public void setReceiverMap(Map<String, String> receiverMap) {
		this.receiverMap = receiverMap;
	}
	public Map<String, String> getThirdPartyMap() {
		return thirdPartyMap;
	}
	public void setThirdPartyMap(Map<String, String> thirdPartyMap) {
		this.thirdPartyMap = thirdPartyMap;
	}
	public String getSenderValidationId() {
		return senderValidationId;
	}
	public void setSenderValidationId(String senderValidationId) {
		this.senderValidationId = senderValidationId;
	}
	public String getReceiverValidationId() {
		return receiverValidationId;
	}
	public void setReceiverValidationId(String receiverValidationId) {
		this.receiverValidationId = receiverValidationId;
	}
	public String getThirdPartyValidationId() {
		return thirdPartyValidationId;
	}
	public void setThirdPartyValidationId(String thirdPartyValidationId) {
		this.thirdPartyValidationId = thirdPartyValidationId;
	}
	
}
