package com.moneygram.dvr.service.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.domain.AttributeBO;
import com.moneygram.dvr.service.constants.DVRConstants;
import com.moneygram.service.dvr_v1.Consumer;

public class ConsumerMapper {

	private static Logger logger = LogFactory.getInstance().getLogger(ConsumerMapper.class);
	static Map<String,String> senderEsbDvrMap = new HashMap<String,String>();
	static Map<String,String> receiverEsbDvrMap = new HashMap<String,String>();
	static Map<String,String> tpSenderEsbDvrMap = new HashMap<String,String>();
	static Map<String,Map<String,String>> consumerMap = new HashMap<String,Map<String,String>>();
	static ConsumerAttributes consumerAttributes = new ConsumerAttributes();

	/* split the birth attributes to day, month and year*/
	private static String splitConsumerBirthAttributes(Consumer consumer)
	{
		String[] birthAttr={"","",""};
		String senderDateOfBirth;
		if(null!=consumer.getBirthDate())
		{
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
			Date date=consumer.getBirthDate().getTime();
			String parseableDate=sdf.format(date);
			birthAttr= parseableDate.split("-");
			senderDateOfBirth=birthAttr[0]+"-"+birthAttr[1]+"-"+birthAttr[2];
			return senderDateOfBirth;
		}
		return null;		
	}

	/* maps the sender object attributes in the DVR to the RVS attributes*/ 
	public static Map<String,String> createSenderMap(Consumer consumer,String attributeXml,ConsumerAttributes consumerAttributes)
	{
		logger.debug("Inside create SenderMap");
		ConsumerMapper consumerMapper = new ConsumerMapper();
		if(null!=consumerAttributes.getConsumerAttributes().get(DVRConstants.SENDER)){
			logger.info("ConsumerAttributes::"+consumerAttributes.getConsumerAttributes().get(DVRConstants.SENDER));
			senderEsbDvrMap = consumerAttributes.getConsumerAttributes().get(DVRConstants.SENDER);
		}
		Map<String,String> attributeMapping= new HashMap<String, String>();
		attributeMapping = consumerMapper.setESBAttributes(consumer,attributeMapping,consumerAttributes,DVRConstants.SENDER);
		return attributeMapping;
	}

	/* maps the receiver object attributes in the DVR to the RVS attributes*/ 
	public static Map<String,String> createReceiverMap(Consumer consumer,String attributeXml,ConsumerAttributes consumerAttributes)
	{
		logger.debug("Inside create receiverMap");	
		Map<String,String> attributeMapping= new HashMap<String, String>();
		ConsumerMapper consumerMapper = new ConsumerMapper();
		if(null!=consumerAttributes.getConsumerAttributes().get(DVRConstants.RECEIVER)){
			logger.info("ConsumerAttributes::"+consumerAttributes.getConsumerAttributes().get(DVRConstants.RECEIVER));
			receiverEsbDvrMap = consumerAttributes.getConsumerAttributes().get(DVRConstants.RECEIVER);
		}
		attributeMapping = consumerMapper.setESBAttributes(consumer,attributeMapping,consumerAttributes,DVRConstants.RECEIVER);
		return attributeMapping;
	}

	/* maps the third party object attributes in the DVR to the RVS attributes*/ 
	public static Map<String,String> createThirdPartyMap(Consumer consumer,String attributeXml,ConsumerAttributes consumerAttributes)
	{
		logger.debug("Inside create ThirdPartyMap");	
		Map<String,String> attributeMapping= new HashMap<String, String>();
		ConsumerMapper consumerMapper = new ConsumerMapper();
		if(null!=consumerAttributes.getConsumerAttributes().get(DVRConstants.THIRDPARTY)){
			logger.info("ConsumerAttributes::"+consumerAttributes.getConsumerAttributes().get(DVRConstants.THIRDPARTY));
			tpSenderEsbDvrMap = consumerAttributes.getConsumerAttributes().get(DVRConstants.THIRDPARTY);
		}
		attributeMapping = consumerMapper.setESBAttributes(consumer,attributeMapping,consumerAttributes,DVRConstants.THIRDPARTY);
		return attributeMapping;
	}

	public static Map<String,String> getOffendingFieldMap()
	{
		Map<String,String> offendingFieldMap= new HashMap<String,String>();
		if(senderEsbDvrMap!=null){
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_FIRST_NAME),DVRConstants.CONSUMER_FIRST_NAME);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_MIDDLE_NAME),DVRConstants.CONSUMER_MIDDLE_NAME);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_LAST_NAME),DVRConstants.CONSUMER_LAST_NAME);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_SECOND_LAST_NAME),DVRConstants.CONSUMER_SECOND_LAST_NAME);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_1),DVRConstants.CONSUMER_ADDRESSLINE_1);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_2),DVRConstants.CONSUMER_ADDRESSLINE_2);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_3),DVRConstants.CONSUMER_ADDRESSLINE_3);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_STATE_PROVINCE),DVRConstants.CONSUMER_STATE_PROVINCE);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_POSTAL_CODE),DVRConstants.CONSUMER_POSTAL_CODE);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_ISO_COUNTRY_CODE),DVRConstants.CONSUMER_ISO_COUNTRY_CODE);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_BIRTH_DATE),DVRConstants.CONSUMER_BIRTH_DATE);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER),DVRConstants.CONSUMER_CELL_PHONE_NUMBER);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER_TYPE),"MOBILE");
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER),DVRConstants.CONSUMER_HOME_PHONE_NUMBER);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER_TYPE),"HOME");
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(senderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_3);
		}
		if(receiverEsbDvrMap!=null){
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_FIRST_NAME),DVRConstants.CONSUMER_FIRST_NAME);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_MIDDLE_NAME),DVRConstants.CONSUMER_MIDDLE_NAME);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_LAST_NAME),DVRConstants.CONSUMER_LAST_NAME);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_SECOND_LAST_NAME),DVRConstants.CONSUMER_SECOND_LAST_NAME);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_1),DVRConstants.CONSUMER_ADDRESSLINE_1);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_2),DVRConstants.CONSUMER_ADDRESSLINE_2);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_3),DVRConstants.CONSUMER_ADDRESSLINE_3);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_STATE_PROVINCE),DVRConstants.CONSUMER_STATE_PROVINCE);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_POSTAL_CODE),DVRConstants.CONSUMER_POSTAL_CODE);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_ISO_COUNTRY_CODE),DVRConstants.CONSUMER_ISO_COUNTRY_CODE);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_BIRTH_DATE),DVRConstants.CONSUMER_BIRTH_DATE);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER),DVRConstants.CONSUMER_CELL_PHONE_NUMBER);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER_TYPE),"MOBILE");
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER),DVRConstants.CONSUMER_HOME_PHONE_NUMBER);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER_TYPE),"HOME");
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(receiverEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_3);
		}
		if(tpSenderEsbDvrMap!=null){
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_FIRST_NAME),DVRConstants.CONSUMER_FIRST_NAME);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_MIDDLE_NAME),DVRConstants.CONSUMER_MIDDLE_NAME);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_LAST_NAME),DVRConstants.CONSUMER_LAST_NAME);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_SECOND_LAST_NAME),DVRConstants.CONSUMER_SECOND_LAST_NAME);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_1),DVRConstants.CONSUMER_ADDRESSLINE_1);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_2),DVRConstants.CONSUMER_ADDRESSLINE_2);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_ADDRESSLINE_3),DVRConstants.CONSUMER_ADDRESSLINE_3);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_STATE_PROVINCE),DVRConstants.CONSUMER_STATE_PROVINCE);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_POSTAL_CODE),DVRConstants.CONSUMER_POSTAL_CODE);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_ISO_COUNTRY_CODE),DVRConstants.CONSUMER_ISO_COUNTRY_CODE);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_BIRTH_DATE),DVRConstants.CONSUMER_BIRTH_DATE);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER),DVRConstants.CONSUMER_CELL_PHONE_NUMBER);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER_TYPE),"MOBILE");
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER),DVRConstants.CONSUMER_HOME_PHONE_NUMBER);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER_TYPE),"HOME");
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_1_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_1);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_2_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_2);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_NUMBER),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_TYPE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_COUNTRYCODE),DVRConstants.CONSUMER_PERSONAL_ID_3);
			offendingFieldMap.put(tpSenderEsbDvrMap.get(DVRConstants.CONSUMER_PERSONAL_ID_3_STATEPROVINCE),DVRConstants.CONSUMER_PERSONAL_ID_3);
		}

		return offendingFieldMap;
	}

	/* set the consumer objects from the map to the attributeBO*/
	public static List<AttributeBO> mapAttributes(Map<String,String> attributeMap,List<AttributeBO> attrList)
	{
		logger.debug(" The attribute map is "+attributeMap);
		for(Entry<String,String> entry:attributeMap.entrySet())
		{
			AttributeBO attributeBO= new AttributeBO();
			attributeBO.setName(entry.getKey());
			attributeBO.setValue(entry.getValue());
			attrList.add(attributeBO);
		}
		return attrList;

	}



	public Map<String,String> setESBAttributes(Consumer consumer,Map<String,String> attributeMapping,ConsumerAttributes consumerAttributes,String validationID)	{

		if(consumer!=null)
		{
			Map<String,String> dvrEsbAttr = consumerAttributes.getConsumerAttributes().get(validationID);
			if(null!=dvrEsbAttr){
				if((null!=consumer.getFirstName()) && (dvrEsbAttr.containsKey(DVRConstants.CONSUMER_FIRST_NAME)) ){
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_FIRST_NAME),consumer.getFirstName().trim());
				}
				if(null!=consumer.getMiddleName() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_MIDDLE_NAME)){
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_MIDDLE_NAME),consumer.getMiddleName().trim());
				}
				if(null!=consumer.getLastName() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_LAST_NAME))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_LAST_NAME),consumer.getLastName().trim());

				if(null!=consumer.getSecondLastName() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_SECOND_LAST_NAME))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_SECOND_LAST_NAME),consumer.getSecondLastName().trim());

				if(null!=consumer.getAddressLine1() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_ADDRESSLINE_1))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_ADDRESSLINE_1),consumer.getAddressLine1().trim());

				if(null!=consumer.getAddressLine2() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_ADDRESSLINE_2))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_ADDRESSLINE_2),consumer.getAddressLine2().trim());
				if(null!=consumer.getAddressLine3() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_ADDRESSLINE_3))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_ADDRESSLINE_3),consumer.getAddressLine3().trim());
				if(null!=consumer.getCity() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_CITY))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_CITY),consumer.getCity().trim());

				if(null!=consumer.getStateProvince() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_STATE_PROVINCE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_STATE_PROVINCE),consumer.getStateProvince().trim());
				if(null!=consumer.getPostalCode() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_POSTAL_CODE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_POSTAL_CODE),consumer.getPostalCode().trim());
				if(null!=consumer.getIsoCountryCode() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_ISO_COUNTRY_CODE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_ISO_COUNTRY_CODE),consumer.getIsoCountryCode().trim());

				String senderDateOfBirth=splitConsumerBirthAttributes(consumer);

				if(null!=senderDateOfBirth && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_BIRTH_DATE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_BIRTH_DATE),senderDateOfBirth.trim());
				if(null!=consumer.getCellPhoneNumber() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_CELL_PHONE_NUMBER)){
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER),consumer.getCellPhoneNumber().trim());
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_CELL_PHONE_NUMBER_TYPE),"MOBILE");
				}
				if(null!=consumer.getHomePhoneNumber() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_HOME_PHONE_NUMBER))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER),consumer.getHomePhoneNumber().trim());
				attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_HOME_PHONE_NUMBER_TYPE),"HOME");



				//personalID1
				if(null!=consumer.getPersonalID1()&& null != consumer.getPersonalID1().getPersonalIDNumber() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_1_NUMBER))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_1_NUMBER),consumer.getPersonalID1().getPersonalIDNumber().trim());
				if(null!=consumer.getPersonalID1() && null != consumer.getPersonalID1().getPersonalIDType()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_1_TYPE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_1_TYPE),consumer.getPersonalID1().getPersonalIDType().trim());
				if(null!=consumer.getPersonalID1() && null != consumer.getPersonalID1().getIsoCountryCode()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_1_COUNTRYCODE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_1_COUNTRYCODE),consumer.getPersonalID1().getIsoCountryCode().trim());
				if(null!=consumer.getPersonalID1()&& null != consumer.getPersonalID1().getStateProvince() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_1_STATEPROVINCE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_1_STATEPROVINCE),consumer.getPersonalID1().getStateProvince().trim());

				//personalID2
				if(null!=consumer.getPersonalID2()&& null != consumer.getPersonalID2().getPersonalIDNumber() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_2_NUMBER))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_2_NUMBER),consumer.getPersonalID2().getPersonalIDNumber().trim());
				if(null!=consumer.getPersonalID2() && null != consumer.getPersonalID2().getPersonalIDType()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_2_TYPE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_2_TYPE),consumer.getPersonalID2().getPersonalIDType().trim());
				if(null!=consumer.getPersonalID2() && null != consumer.getPersonalID2().getIsoCountryCode()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_2_COUNTRYCODE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_2_COUNTRYCODE),consumer.getPersonalID2().getIsoCountryCode().trim());
				if(null!=consumer.getPersonalID2()&& null != consumer.getPersonalID2().getStateProvince() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_2_STATEPROVINCE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_2_STATEPROVINCE),consumer.getPersonalID2().getStateProvince().trim());

				//personalID3
				if(null!=consumer.getPersonalID3()&& null != consumer.getPersonalID3().getPersonalIDNumber() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_3_NUMBER))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_3_NUMBER),consumer.getPersonalID3().getPersonalIDNumber().trim());
				if(null!=consumer.getPersonalID3() && null != consumer.getPersonalID3().getPersonalIDType()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_3_TYPE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_3_TYPE),consumer.getPersonalID3().getPersonalIDType().trim());
				if(null!=consumer.getPersonalID3() && null != consumer.getPersonalID3().getIsoCountryCode()&& dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_3_COUNTRYCODE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_3_COUNTRYCODE),consumer.getPersonalID3().getIsoCountryCode().trim());
				if(null!=consumer.getPersonalID3()&& null != consumer.getPersonalID3().getStateProvince() && dvrEsbAttr.containsKey(DVRConstants.CONSUMER_PERSONAL_ID_3_STATEPROVINCE))
					attributeMapping.put(dvrEsbAttr.get(DVRConstants.CONSUMER_PERSONAL_ID_3_STATEPROVINCE),consumer.getPersonalID3().getStateProvince().trim());
			}
		}
		return attributeMapping;

	}

/*	private static List<String> getPropOrdersForClass(Class className){
		if (className == null)   return null;
		XmlType xmlType=(XmlType)className.getAnnotation(javax.xml.bind.annotation.XmlType.class);
		String[] propOrders=xmlType.propOrder();
		List<String> propList = Arrays.asList(propOrders);
		return propList;
	}
*/
}
